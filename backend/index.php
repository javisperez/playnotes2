<?php
require_once 'API.class.php';
require_once 'DB.class.php';
require_once 'playnotes.class.php';

// Requests from the same server don't have a HTTP_ORIGIN header
if (!array_key_exists('HTTP_ORIGIN', $_SERVER)) {
    $_SERVER['HTTP_ORIGIN'] = $_SERVER['SERVER_NAME'];
}

$queryOffset = strlen($_SERVER['REQUEST_URI']);

// Parse the Route into an array
$route = substr($_SERVER['REQUEST_URI'], 1, $queryOffset);

try {

    if (!strlen(trim($route))) {
        throw new Exception('No api entry, everything ok?');
    }

    $API = new Playnotes($route, $_SERVER['HTTP_ORIGIN']);

    ob_start('ob_gzhandler');
    echo $API->processAPI();
    ob_end_flush();

} catch (Exception $e) {
    echo json_encode(Array('error' => $e->getMessage()));
}
