<?php
/*
* Mysql database class - only one connection alowed
*
* Taken from:
* https://gist.github.com/jonashansen229/4534794
*/

class DB {

    private $_connection;

    private static $_instance; //The single instance

    private $_host = "159.203.99.24"; // "10.132.19.184";

    private $_username = "letsplaynotes";

    private $_password = "Z&~{9(.Fe<91$[m";

    private $_database = "letsplaynotes";

    /*
    Get an instance of the Database
    @return Instance
    */
    public static function getInstance() {
        if(!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    // Constructor
    private function __construct() {
        $this->_connection = new mysqli($this->_host, $this->_username, $this->_password, $this->_database);

        // Error handling
        if(mysqli_connect_error()) {
            trigger_error("Failed to connect to MySQL: " . mysqli_connect_error(), E_USER_ERROR);
        }
    }
    // Magic method clone is empty to prevent duplication of connection
    private function __clone() { }

    // Get mysqli connection
    public function getConnection() {
        return $this->_connection;
    }
}
