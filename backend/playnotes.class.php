<?php

class Playnotes extends API {

    private $dbInstance = null;

    private $db = null;

    public function __construct($request, $origin) {
        parent::__construct($request);

        $this->dbInstance = DB::getInstance();

        $this->db = $this->dbInstance->getConnection();
    }

    /**
     * Insturments
     */
     protected function instrument($args) {
        if ($this->method != 'GET') {
            throw new Exception("Method not allowed");
            return;
        }

        $result = $this->db->query('SELECT id, data FROM instruments WHERE instrument = "'.$args[0].'"');

        $data = [];

        $row = null;

        if ($result) {
            $row = $result->fetch_object();

            $row->data = json_decode($row->data);
        }

        return $row;
    }

    /**
     * Metronome
     */
    protected function metronome() {
        if ($this->method != 'GET') {
            throw new Exception("Method not allowed");
            return;
        }

        return $this->instrument(['metronome']);
    }

    /**
     * Tracks
     */
    protected function track($args) {
        // GET /track/:id
        if ($this->method == 'GET') {
            $result = $this->db->query('SELECT data FROM tracks WHERE id = "'.$args[0].'"');

            $data = [];

            $row = null;

            if ($result) {
                $row = $result->fetch_object();

                if ($row->data) {
                    $row->data = json_decode($row->data);
                }
            }

            return $row;
        }
        // POST /track
        elseif ($this->method == 'POST') {
            $this->db->query('INSERT INTO tracks (data, id_instrument, created_at) VALUES (\''.json_encode($this->payload->data).'\', '.$this->payload->instrument.', NOW())');

            return ['id' => $this->db->insert_id];
        }
    }
}
