#!/bin/bash

# UNIX SETUP
# ------------------------------------
# nodejs     - http://nodejs.org/
# gzip       - http://www.gzip.org/
# fluidsynth - http://sourceforge.net/apps/trac/fluidsynth/
# oggenc     - http://www.rarewares.org/ogg-oggenc.php
# lame       - http://lame.sourceforge.net/
# sox        - http://sox.sourceforge.net/soxi.html

# MODIFIED BY JAVIS PEREZ TO WORK ON MAC ENVIRONMENT
# Original credits to midipiano.js
# ------------------------------------
# Please use BREW to install dependencies

QUALITY=32;
FADEOUT="0:0";
LENGTH=3;
FORMAT="mp3";
GAIN=0.5;
OFFSET=0;
OUTPUT="none";

#
# Argument's list
#
while [[ $# > 1 ]]
do
  key="$1"

  case $key in

    -i|--input)
      INPUT="$2"
    shift
    ;;

    -o|--output)
      OUTPUT="$2"
    shift
    ;;

    -fo|--fadeout)
      FADEOUT="$2"
    shift
    ;;

    -f|--format)
      FORMAT="$2"
    shift
    ;;

    -q|--quality)
      QUALITY="$2"
    shift
    ;;

    -l|--length)
      LENGTH="$2"
    shift
    ;;

    -g|--gain)
      GAIN=$2
    shift
    ;;

    -of|--offset)
      OFFSET=$2
    shift
    ;;

    --default)
      DEFAULT=YES
    ;;
    *)

    # unknown option
    ;;
  esac

  shift
done

# build directory
BUILDDIR="./build";

# export directory
EXPORTDIR="$BUILDDIR/export";

if [ $OUTPUT = "none" ]; then
    OUTPUT=${INPUT};
fi

# if the export dir doesnt exists, create it...
if [ ! -d "$EXPORTDIR" ]; then
  mkdir -p $EXPORTDIR;
fi

# also the midi tone's directory
MIDIDIR="$BUILDDIR/tones_midis";
if [ ! -d "$MIDIDIR" ]; then
    mkdir -p $MIDIDIR;
fi

# soundfont file to map
SOUNDFONT="./soundfonts/${INPUT}.sf2";

# create MIDI files for audible notes
node "./scripts/gen-midi.js" ${OFFSET};

# set the final file path and name
if [ $FORMAT = "ogg" ]; then
    FINALFILE="$EXPORTDIR/${OUTPUT}.o.json";
elif [ $FORMAT = "mp3" ]; then
    FINALFILE="$EXPORTDIR/${OUTPUT}.m.json";
else
    echo "Invalid format, choose either mp3 or ogg";
    exit;
fi

# write the headers
echo '{' > "${FINALFILE}";

# from MIDI to WAV to OGG to JS to JGZ, and beyond!
find $MIDIDIR -name '*.midi' -print0 | while read -d $'\0' file

do
    echo "GAIN: ${GAIN}";
    # from MIDI to WAV
    ./bin/fluidsynth/fluidsynth -a "file" -v -d -C 1 -R 1 -g ${GAIN} -T "wav" -F "$file.tmp.wav" "$SOUNDFONT" "$file";

    if [ $FADEOUT != "0:0" ]; then
        SOXLENGTH=`soxi -d "$file.tmp.wav"`;

        echo "AUDIO LENGTH: ${SOXLENGTH}";
        sox "$file.tmp.wav" "$file.wav" fade t "0:0" $SOXLENGTH $FADEOUT;
    else
        mv "$file.tmp.wav" "$file.wav";
    fi

    # from WAV to OGG
    if [ $FORMAT = "ogg" ]; then
        OGGFILE=`echo ${file%.midi}.ogg`;

        oggenc -m 32 -M ${QUALITY} "$file.wav";

        mv "$file.ogg" "$OGGFILE";

        # from OGG to base64 embedded in Javascript
        JSCONTENT="\"`basename \"${file%.midi}\"`\": \"`base64 \"$OGGFILE\"`\",";

        echo ${JSCONTENT} >> ${FINALFILE};

        `rm "$OGGFILE"`;

    # from WAV to MP3
    elif [ $FORMAT = "mp3" ]; then
        MP3FILE=`echo "${file%.midi}.mp3"`;

        #--noreplaygain --silent

        lame -v -m m -b 8 -B ${QUALITY} "$file.wav" "$MP3FILE";

        # from MP3 to base64 embedded in Javascript
        JSCONTENT="\"`basename \"${file%.midi}\"`\": \"`base64 \"$MP3FILE\"`\",";

        echo ${JSCONTENT} >> ${FINALFILE};

        # `rm "$MP3FILE"`;
    fi

    # cleanup
    # rm "$file";
    # rm "$file.tmp.wav";
    # rm "$file.wav";
done

# remove the last comma
sed -i '' '$ s/.$//' "${FINALFILE}";

# write the footers
echo '}' >> ${FINALFILE};

gzip ${FINALFILE} -c > "${FINALFILE}.gzip"
