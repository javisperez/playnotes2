'use strict';

angular.module('header')

    .service('HeaderService', function (store) {

        this.runTrackbar = false;

        this.trackRunDuration = 10;

        this.isReady = false;

        this.statusMsg = 'Loading...';

        this.keyboardShortcutActive = store.get('keyboard-shortcut-active');

        if (this.keyboardShortcutActive === null) {
            this.keyboardShortcutActive = true;
        }

        this.scrollable = true;
    });
