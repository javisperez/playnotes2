'use strict';

angular.module('header')

    .controller('HeaderController', function ($scope, InstrumentService, Audio, Media, Metronome, Track, $timeout, $location, HeaderService, store) {

        $scope.activeControl = null;

        $scope.metronomeIsRunning = false;

        $scope.currentInstrument = 'full_grand_piano';

        $scope.instruments = ['full_grand_piano', 'acoustic_steel_guitar', 'acoustic_nylon_guitar', 'basic_bass', 'heavy_guitar', 'drums', 'trumpet', 'saxophone'];

        $scope.url = $location.absUrl();

        $scope.layersAreVisible = false;

        $scope.layers = [1];

        $scope.currentLayer = 0;

        $scope.status = null;

        // Data binding between header service and controller
        ['runTrackbar', 'trackRunDuration', 'isReady', 'statusMsg', 'keyboardShortcutActive'].forEach(function (item) {
            Object.defineProperty($scope, item, {
                get: function () { return HeaderService[item]; },
                set: function (val) { HeaderService[item] = val; }
            });
        });

        Object.defineProperty($scope, 'bpm', {
            get: function () { return Metronome.bpm; },
            set: function (val) { Metronome.bpm = val; }
        });

        var autoStopTimer = null,
            metronomeSoundIsLoaded = false,
            lastLayerAdded = 1;

        function stopTrackbar() {
            $scope.activeControl = null;
            $scope.runTrackbar = false;
        }

        function togglePlaying() {
            // Disable the auto stop timer for the progress bar
            $timeout.cancel(autoStopTimer);

            Media.togglePlaying();

            if (!Media.isPlaying) {
                stopTrackbar();
                return;
            }

            var trackDuration = 0;

            for (var i=0,l=Media.track.length; i<l; i++) {
                var trackKeys      = Object.keys(Media.track[i].data),
                    lastTrackKey   = trackKeys[trackKeys.length-1],
                    _trackDuration = parseInt(lastTrackKey) + parseInt(Media.track[i].data[lastTrackKey].duration);

                if (trackDuration < _trackDuration) {
                    trackDuration = _trackDuration;
                }
            }

            HeaderService.trackRunDuration = Math.ceil(trackDuration / 1000);

            // $scope.runTrackbar = true;
            HeaderService.runTrackbar = true;

            autoStopTimer = $timeout(function () {
                $scope.togglePlaying();
            }, trackDuration+1000);

            $scope.activeControl = 'play';
        }

        HeaderService.togglePlay = togglePlaying;

        // Set Instrument
        $scope.setInstrument = function (instrument) {
            if ($scope.status) {
                return;
            }

            if ($scope.currentInstrument === instrument) {
                return;
            }

            $scope.status = instrument;

            HeaderService.scrollable = true;

            $scope.currentInstrument = instrument;

            InstrumentService.set(instrument).then(function (response) {
                Audio.setData(response.label, response.data);

                $scope.status = instrument+'-done';

                $timeout(function () {
                    $scope.status = null;
                    $scope.instrumentsScreenIsVisible = false;
                }, 1000);
            });
        };

        $scope.toggleKeyboardShortcut = function () {
            HeaderService.keyboardShortcutActive = !HeaderService.keyboardShortcutActive;
            store.set('keyboard-shortcut-active', HeaderService.keyboardShortcutActive);
        };

        // The change instrument modal screen
        $scope.instrumentSelector = function () {
            $scope.instrumentsScreenIsVisible = !$scope.instrumentsScreenIsVisible;
            HeaderService.scrollable = !$scope.instrumentsScreenIsVisible;
        };

        // Toggle Recording
        $scope.toggleRecording = function () {
            if (Media.isPlaying) {
                $scope.togglePlaying();
            }

            Media.toggleRecording();

            if (!Media.isRecording) {
                $scope.activeControl = null;
                return;
            }

            $scope.activeControl = 'record';
        };

        // Toggle Play
        $scope.togglePlaying = togglePlaying;

        // Send the track to the server to save it
        $scope.saveTrack = function () {
            if (!$scope.hasSomethingRecorded()) {
                return;
            }

            var _track = Media.track,
                track = new Track();

            track.data = _track;
            track.instrument = InstrumentService.current.id;

            track.$save().then(function (data) {
                $location.url('/?t='+data.id);
                $scope.url = $location.absUrl();
                $scope.showModal = true;
            });
        };

        // Set the metronome on/off
        $scope.toggleMetronome = function () {
            $scope.metronomeIsRunning = !$scope.metronomeIsRunning;

            if ($scope.metronomeIsRunning) {
                Metronome.play();
                return;
            }

            Metronome.stop();
        };

        // Is there anything recorded?
        $scope.hasSomethingRecorded = function () {
            return !!Object.keys(Media.track[$scope.currentLayer].data).length;
        };

        // Load the metronome sound on request, useful to prevent heavy initial load
        $scope.loadMetronomeSound = function () {
            if (metronomeSoundIsLoaded) {
                return;
            }

            Metronome.loadSounds();
            metronomeSoundIsLoaded = true;
        };

        $scope.toggleLayers = function () {
            $scope.layersAreVisible = !$scope.layersAreVisible;
        };

        $scope.trackHasLayer = function (layer) {
            return !!Media.track[layer];
        };

        // Track layers
        $scope.useLayer = function (layer) {
            $scope.currentLayer = layer;

            Media.useLayer($scope.currentLayer);
        };

        $scope.closeLayerMenu = function () {
            // Close the menu
            $scope.layersAreVisible = false;
        };

        $scope.addLayer = function () {
            lastLayerAdded = $scope.layers.length + 1;

            $scope.layers.push(lastLayerAdded);

            Media.track.push({
                instrument: $scope.currentInstrument,
                data: {}
            });

            $scope.useLayer(Media.track.length-1);
        };

        $scope.removeLayer = function (layer) {
            $scope.layers.splice(layer, 1);

            if ($scope.currentLayer === Media.track.length-1) {
                $scope.currentLayer--;
                Media.useLayer($scope.currentLayer);
            }

            Media.track.splice(layer, 1);
        };

        $scope.playLayer = function (layer) {
            console.log(layer);
        };

    });
