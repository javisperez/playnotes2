'use strict';

angular.module('config', [])

    .provider('apiUrl', function () {

        this.$get = function () {

            return function (uri) {

                return 'http://api.letsplaynotes.com/' + uri;

            };
        };
    });
