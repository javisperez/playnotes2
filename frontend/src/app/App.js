'use strict';

angular.module('playnotes', [
    // 'ngAnimate',
    'ngCookies',
    'ngTouch',
    'ngSanitize',
    'ngResource',
    'ngRoute',
    'mousetrap',
    '720kb.socialshare',
    'angular-capitalize-filter',
    'angular-storage',

    // Modules
    'header',
    'footer',
    'instruments',
    'audio',
    'media',
    'metronome'
])
    .config(function ($routeProvider) {

        $routeProvider.otherwise({
             redirectTo: '/'
        });

    })

    .controller('MainCtrl', function ($scope, HeaderService) {

        Object.defineProperty($scope, 'noScroll', {
            get: function () { return !HeaderService.scrollable; }
        });

    });
