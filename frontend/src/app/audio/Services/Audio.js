'use strict';

angular.module('audio')

    .service('Audio', function (base64binary, BuffersService) {

        var ctx = new AudioContext(),
            data = null,
            gains = {},
            sources = {};

        this.buffers = {};

        this.setData = function (instrument, _data) {
            data = _data;

            angular.forEach(data, function (base64, tone) {
                var buffer = base64binary.decodeArrayBuffer(base64);

                ctx.decodeAudioData(buffer, function (audioBuffer) {
                    audioBuffer.id = tone;
                    // that.buffers[tone] = audioBuffer;
                    BuffersService.set(instrument, tone, audioBuffer);
                });
            });
        };

        this.play = function (instrument, tone, volume) {
            var source    = ctx.createBufferSource(),
                gainNode  = ctx.createGain(),
                localGain = volume / 100;

            source.buffer = BuffersService.get(instrument, tone);

            source.connect(gainNode);

            gainNode.connect(ctx.destination);

            gainNode.gain.value = localGain;

            gainNode.gain.linearRampToValueAtTime(localGain, ctx.currentTime + 0.2);

            gains[tone]   = gainNode;
            sources[tone] = source;

            source.start(0);
        };

        this.stop = function (tone) {
            var source = sources[tone],
                startTime = ctx.currentTime,
                gainNode = gains[tone],
                duration = 0.6;

            if (!source) {
                return;
            }

            gainNode.gain.linearRampToValueAtTime(0, startTime + duration - 0.2);

            source.stop(startTime + duration);
        };

    });
