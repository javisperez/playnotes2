'use strict';

angular.module('audio')

    .service('BuffersService', function () {

        this.data = {};

        this.set = function (instrument, key, data) {
            if (!this.data[instrument]) {
                this.data[instrument] = {};
            }

            this.data[instrument][key] = data;
        };

        this.get = function (instrument, key) {
            return this.data[instrument][key];
        };

        this.hasInstrument = function (instrument) {
            return !!this.data[instrument];
        };

    });
