'use strict';

angular.module('metronome')

    .service('Metronome', function (MetronomeSound) {

        this.bpm = 120;

        this.sounds = null;

        var audioPlayerLo = new Audio(),
            audioPlayerHi = new Audio(),
            ticks = 0,
            top = this,
            metronomeTimeout = null;

        audioPlayerLo.volume = 0.8;

        this.loadSounds = function () {
            MetronomeSound.get().$promise.then(function (data) {
                audioPlayerLo.src = data.low;
                audioPlayerHi.src = data.high;
                top.sounds = data;
            });
        };

        this.play = function () {
            top.bpm = Math.max(30, Math.min(450, top.bpm || 30));

            var bpm = 60 / top.bpm * 1000;

            audioPlayerLo.pause();
            audioPlayerHi.pause();

            if (ticks < 3) {
                audioPlayerLo.play();

                metronomeTimeout = setTimeout(top.play, bpm);

                ticks++;
                return;
            }

            audioPlayerHi.play();

            metronomeTimeout = setTimeout(top.play, bpm);

            ticks = 0;
        };

        this.stop = function () {
            ticks = 0;
            clearTimeout(metronomeTimeout);
            audioPlayerLo.pause();
            audioPlayerHi.pause();
        };
    });
