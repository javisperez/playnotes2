'use strict';

angular.module('metronome')

    .factory('MetronomeSound', function ($resource, apiUrl) {

        var Resource = $resource(apiUrl('metronome'), {}, {

            get: {
                method: 'GET',
                interceptor: {
                    response: function (response) {
                        return response.data.data;
                    }
                }
            },

            query: {
                method: 'GET',
                isArray: true,
                cache: true
            },

            update: {
                method: 'PUT'
            }
        });

        return Resource;
    });
