'use strict';

angular.module('playnotes')

    .factory('Shortcuts', function () {

        // Keys in the order:
        // TONE: SHORTCUT
        var keys = {
                // 1st Octave
                'C2': 'shift+q', 'C2#': 'shift+1', 'D2': 'shift+w', 'D2#': 'shift+2', 'E2': 'shift+e', 'F2': 'shift+r', 'F2#': 'shift+3', 'G2': 'shift+t', 'G2#': 'shift+4', 'A2': 'shift+y', 'A2#': 'shift+5',  'B2': 'shift+u',
                // 2nd Octave
                'C3': 'q',       'C3#': '1',       'D3': 'w',       'D3#': '2',       'E3': 'e',       'F3': 'r',       'F3#': '3',       'G3': 't',       'G3#': '4',       'A3': 'y',       'A3#': '5',        'B3': 'u',
                // 3rd Octave
                'C4': 'i,a',       'C4#': '6',       'D4': 'o,s',       'D4#': '7',       'E4': 'p,d',       'F4': 'f',       'F4#': '8',       'G4': 'g',       'G4#': '9',       'A4': 'h',       'A4#': '0',        'B4': 'j',
                // 4th Octave
                'C5': 'k,z',       'C5#': '-',       'D5': 'l,x',       'D5#': '=',       'E5': ';,c',       'F5': 'v',       'F5#': '[',       'G5': 'b',       'G5#': ']',       'A5': 'n',       'A5#': '\\',       'B5': 'm',
                // 5th Octave
                'C6': 'shift+z', 'C6#': 'shift+-', 'D6': 'shift+x', 'D6#': 'shift+=', 'E6': 'shift+c', 'F6': 'shift+v', 'F6#': 'shift+[', 'G6': 'shift+b', 'G6#': 'shift+]', 'A6': 'shift+n', 'A6#': 'shift+\\', 'B6': 'shift+m'
            };

        return keys;
    });
