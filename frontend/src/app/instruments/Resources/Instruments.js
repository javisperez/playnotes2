'use strict';

angular.module('instruments')

    .factory('Instrument', function ($resource, apiUrl) {

        var Resource = $resource(apiUrl('instrument/load/:instrument'), {}, {

            get: {
                method: 'GET',
                cache: true,
                interceptor: {
                    response: function (response) {
                        return response.data;
                    }
                }
            },

            query: {
                method: 'GET',
                isArray: true,
                cache: true
            },

            update: {
                method: 'PUT'
            }
        });

        return Resource;
    });
