'use strict';

angular.module('instruments')

    .factory('Notes', function (Key, Shortcuts) {
        var notes = ['C','C#','D','D#','E','F','F#','G','G#','A','A#','B'],
            result = [];

        // how many octaves?
        for (var i=2; i<7; i++) {

            // for each note, add the octave
            for (var note in notes) {

                var tmp = notes[note] + i,
                    key = new Key.Instance({
                        tone: tmp.replace('#'+i, i+'#'),
                        label: tmp.replace('#', 'Sharp'),
                        isSemitone: tmp.indexOf('#') > -1,
                        pressed: false,
                        volume: 50
                    }),
                    splittedShortcut = Shortcuts[key.note.tone].split(',');

                key.setShortcut(splittedShortcut);

                result.push(key);
            }

        }

        return {
            all: function () {
                return result;
            },

            fromTone: function (tone) {
                var filtered = result.filter(function (key) {
                    return key.tone === tone;
                });

                return filtered[0];
            },

            tones: function () {
                var tones = result.filter(function (note) {
                    return note.key.indexOf('#') === -1;
                });

                return tones;
            },

            semitones: function () {
                var semitones = result.filter(function (note) {
                    return note.key.indexOf('#') !== -1;
                });

                return semitones;
            }
        };
    });
