'use strict';

angular.module('instruments')

    .service('InstrumentService', function (Instrument, $q) {

        var top = this;

        this.current = {
            id: 0,
            data: null,
            label: 'full_grand_piano'
        };

        this.queue = null;

        this.currentLabel = null;

        this.instrumentsScreenIsVisible = false;

        this.set = function (instrument) {

            return $q(function (resolve, reject) {
                if (top.queue === instrument) {
                    return;
                }

                top.queue = instrument;

                Instrument.get({
                    instrument: instrument
                }).$promise.then(function (data) {
                    data.label = instrument;

                    top.current = data;

                    top.queue = null;

                    resolve(data);
                }, function (data) {
                    data.label = instrument;

                    reject(data);
                });
            });
        };


        this.buffer = function (instrument) {

            return $q(function (resolve, reject) {
                if (top.queue === instrument) {
                    return;
                }

                top.queue = instrument;

                Instrument.get({
                    instrument: instrument
                }).$promise.then(function (data) {
                    data.label = instrument;

                    top.queue = null;

                    resolve(data);
                }, function (data) {
                    data.label = instrument;

                    reject(data);
                });
            });

        };

    });
