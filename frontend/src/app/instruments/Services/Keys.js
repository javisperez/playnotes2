'use strict';

angular.module('instruments')

    .service('Key', function (Audio, Media, InstrumentService, HeaderService) {

        function Instance(note) {
            this.note = note;
            this.hasKeyDown = false;
        }

        Instance.prototype.release = function () {
            if (!HeaderService.isReady) {
                return;
            }

            Audio.stop(this.note.tone);

            this.note.lastReleaseTime = (+new Date());

            // Duration of the note pressed, for recording and dynamic volume
            this.note.duration = (+new Date()) - this.note.pressedTime;

            Media.recordKey(this.note);

            this.note.pressed = false;
        };

        Instance.prototype.press = function (instrument) {
            if (!HeaderService.isReady) {
                return;
            }

            var timeDelta = (+new Date()) - this.note.lastReleaseTime;

            // The duration of the pressed note
            this.note.pressedTime = (+new Date());

            this.note.volume = 50;

            if (timeDelta < 110) {
                this.note.volume = Math.min(127, 80 + Math.round( ((110 - timeDelta) / 110) * 100 ));
            }

            Audio.play(instrument, this.note.tone, this.note.volume);

            this.note.pressed = true;
        };

        Instance.prototype.keydown = function () {
            var that = this,
                obj = {};

            for (var hotkey in that.shortcut) {
                obj[that.shortcut[hotkey]] = function () {
                    if (!that.note.pressed) {
                        that.press.call(that, InstrumentService.current.label);
                    }
                };
            }

            return obj;
        };

        Instance.prototype.keyup = function () {
            var that = this,
                obj = {};

            for (var hotkey in that.shortcut) {
                obj[that.shortcut[hotkey]] = function () {
                    that.hasKeyDown = false;
                    that.release.call(that, null);
                };
            }

            return obj;
        };

        Instance.prototype.setShortcut = function (shortcut) {
            this.shortcut = shortcut;
        };

        return {
            Instance: Instance
        };
    });
