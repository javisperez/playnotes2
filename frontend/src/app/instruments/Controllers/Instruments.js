'use strict';

angular.module('instruments')

    .controller('InstrumentsController', function ($scope, notes, Instrument, Track, Audio, InstrumentService, $location, Media, BuffersService, HeaderService, Shortcuts) {

        // Instrument notes
        $scope.notes = notes;

        $scope.isMouseDown = false;

        HeaderService.isReady = false;
        HeaderService.statusMsg = 'Getting ready...';

        Object.defineProperty($scope, 'keyboardShortcutActive', {
            get: function () { return HeaderService.keyboardShortcutActive; },
            set: function (val) { HeaderService.keyboardShortcutActive = val; }
        });

        // Set the default instrument
        InstrumentService.set('full_grand_piano').then(function (instrument) {
            Audio.setData(instrument.label, instrument.data);

            HeaderService.statusMsg = 'Done, enjoy!';
            HeaderService.isReady = true;
        });

        // Handle query strings
        var qs = $location.search();

        // If there's a track to load, get it
        // and all the dependency instruments
        if (Object.keys(qs).length) {

            HeaderService.isReady = false;
            HeaderService.statusMsg = 'Getting song info...';

            Track.get({
                    track_id: qs.t
                }).$promise.then(function (track) {
                    if (!track.data) {
                        HeaderService.statusMsg = 'Track not found :(';
                        return;
                    }

                    for (var i =0,l=track.data.length; i<l; i++) {

                        var _track = track.data[i],
                            j = i + 1;

                        HeaderService.statusMsg = 'Getting tracks...';

                        if (!BuffersService.hasInstrument(_track.instrument)) {
                            HeaderService.isReady = false;

                            // Encapsulating to keep sync track of the current status msg
                            (function (_j, _l, _track) {

                                InstrumentService.buffer(_track.instrument).then(function (instrument) {
                                    Audio.setData(instrument.label, instrument.data);

                                    if (instrument.label === 'full_grand_piano') {
                                        InstrumentService.current = instrument;
                                    }

                                    if (_j === _l) {
                                        HeaderService.statusMsg = 'Done, enjoy!';
                                        HeaderService.isReady = true;
                                        HeaderService.togglePlay();
                                    }
                                });

                            })(j, l, _track);
                        }
                    }

                    Media.track = track.data;
                });
        }

        $scope.shortcuts = Shortcuts;

        $scope.press = function (key) {
            if (!HeaderService.isReady) {
                return;
            }

            $scope.isMouseDown = true;

            key.press(InstrumentService.current.label);
        };

        $scope.release = function (key) {
            if (!HeaderService.isReady) {
                return;
            }

            $scope.isMouseDown = false;

            key.release();
        };

        $scope.enter = function (key) {
            if (!$scope.isMouseDown || !HeaderService.isReady) {
                return;
            }

            key.press(InstrumentService.current.label);
        };

        $scope.leave = function (key) {
            if (!$scope.isMouseDown || !HeaderService.isReady) {
                return;
            }

            key.release();
        };

    });
