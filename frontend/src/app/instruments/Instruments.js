'use strict';

angular.module('instruments', [
    'config'
])

    .config(function ($routeProvider) {

        $routeProvider.when('/', {
            templateUrl: 'app/instruments/Views/Instruments.html',
            controller: 'InstrumentsController',
            reloadOnSearch: false,
            resolve: {
                notes: function (Notes) {
                    return Notes.all();
                }
            }
        });

    });
