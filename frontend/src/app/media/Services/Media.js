'use strict';

angular.module('media')

    .service('Media', function (Audio, InstrumentService) {

        this.isRecording = false;
        this.isPlaying   = false;
        this.track       = [{
            instrument: InstrumentService.current.label,
            data: {}
        }];
        this.currentLayer = 0;

        var recordingTime = null,
            queue = [],
            nextStopTimeout  = null,
            nextStartTimeout = null,
            top = this;

        // Helper function to prevent jsHint errors
        function playTone(instrument, key, currentQueueEntry) {

            return function () {

                key.pressed = true;

                Audio.play(instrument, key.tone, 50);

                nextStartTimeout = currentQueueEntry;

                // Update the UI
                // @TODO find a better way to do this, by updating the "pressed" property of the key object
                angular.element('.'+key.label).addClass('pressed');

                // Set the stop timer within the key duration
                nextStopTimeout = setTimeout(function () {

                    Audio.stop(key.tone);

                    key.pressed = false;

                    // @TODO same here
                    angular.element('.'+key.label).removeClass('pressed');

                }, key.duration);
            };
        }

        this.toggleRecording = function () {
            if (!top.isRecording) {
                if (Object.keys(top.track[top.currentLayer].data).length) {
                    if (!confirm('The current track you have recorded will be deleted, are you sure?')) {
                        return;
                    }
                }
            }

            top.isRecording = !top.isRecording;

            if (top.isRecording) {
                top.track[top.currentLayer].instrument = InstrumentService.current.label;

                // Clear the track
                top.track[top.currentLayer].data = {};

                top.togglePlaying();

                // Set the recording start time
                recordingTime = (+new Date());
            }
        };

        this.togglePlaying = function () {
            top.isPlaying = !top.isPlaying;
            // top.isRecording = false;

            var i,l;

            // STOP! Hammer time!
            if (!top.isPlaying) {
                clearTimeout(nextStopTimeout);

                for (i=0, l=queue.length; i<l; i++) {
                    clearTimeout(queue[i]);
                }

                angular.element('li.pressed').removeClass('.pressed');

                queue = [];

                return;
            }

            for (i=0, l=top.track.length; i<l; i++) {
                for (var startTime in top.track[i].data) {
                    var key = top.track[i].data[startTime],
                        instrument = top.track[i].instrument;

                    queue.push(setTimeout(playTone(instrument, key, queue.length), startTime));

                    angular.element('li.pressed').removeClass('.pressed');
                }
            }
        };

        this.useLayer = function (layer) {
            top.currentLayer = layer;
        };

        this.recordKey = function (key) {
            if (!top.isRecording) {
                return;
            }

            key.pressedTime = key.pressedTime - recordingTime;

            top.track[top.currentLayer].instrument = InstrumentService.current.label;

            top.track[top.currentLayer].data[key.pressedTime] = {
                tone: key.tone,
                label: key.label,
                duration: key.duration
            };

            // Reset it so its recordable again
            key.pressedTime = null;
        };

    });
