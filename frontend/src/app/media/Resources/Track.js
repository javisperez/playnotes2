'use strict';

angular.module('playnotes')

    .factory('Track', function ($resource, apiUrl) {

        var Resource = $resource(apiUrl('track/:track_id'), {}, {

            get: {
                method: 'GET'
            },

            query: {
                method: 'GET',
                isArray: true,
                cache: true
            },

            update: {
                method: 'PUT'
            }
        });

        return Resource;
    });
